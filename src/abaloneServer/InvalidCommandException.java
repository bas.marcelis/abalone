package abaloneServer;

public class InvalidCommandException extends Exception {
	
	public InvalidCommandException() {
		super("Invalid Command");
	}
}