package abaloneServer;

import abalone.Move;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private Socket client;
    private BufferedReader in;
    private PrintWriter out;
    private gameManager gamemanager;
	
    private String username;
	
    private GameObject currentGameObject;
	
    /**
    * This is the clienthandler we used.
    * It creates a client with it's socket and uses gamemanager as a controller object.
    * @param clientSocket the socket which this client uses
    * @param gamemanager a manager object like a controller in the MVC pattern.
    * @throws IOException an exception is throw when there is an error.
    */
    
    public ClientHandler(Socket clientSocket, gameManager gamemanager) throws IOException {
		this.client = clientSocket;
		this.gamemanager = gamemanager;
		in = new BufferedReader(new InputStreamReader(client.getInputStream()));
		out = new PrintWriter(client.getOutputStream(), true);
	}
	
	@Override
	public void run() {

		try {
			while (true) {
				try {
					String commandString = in.readLine();
					String[] command = commandString.split(";");
					
					if (command.length == 0) {
						throw new InvalidCommandException();
					}
					
					//CREATE GAME
					if (command[0].contentEquals("CREATEGAME")) {
						try {
							
							if (command[1] == null || command[2] == null || command[3] == null) {
								throw new InvalidCommandException();
							}
							
							int playerAmount = Integer.parseInt(command[1]);
							if (playerAmount < 2 || playerAmount > 4) {
								throw new InvalidCommandException();
							}
							
							username = command[3];
							
							if (!gamemanager.getAllGameNames().contains("command[2]")) {
								gamemanager.newGame(playerAmount, command[2], command[3], this);
								currentGameObject = gamemanager.getGameObject(command[2]);
								currentGameObject.joinGame(this, username);
							} else {
								out.println("ERROR;Gamename already exists");
							}
							
						} catch (NumberFormatException e) {
							throw new InvalidCommandException();
						}
					}
					
					//FIND GAMES
					else if (command[0].contentEquals("FINDGAMES")) { 
						String result = "";
						for (String gamename : gamemanager.getJoinableGames()) {
							result = gamename + ";";
						}
						out.println(result);
					}
					
					//JOIN A GAME
					else if (command[0].contentEquals("JOINGAME")) {

						if (command[1] == null || command[2] == null) {
							throw new InvalidCommandException();
						}
						username = command[2];
						
						if (gamemanager.getAllGameNames().contains(command[1])) {
							gamemanager.getGameObject(command[1]).joinGame(this, username);
						} else {
							out.println("ERROR;Game does not exist");
						}
						
					} else {
						out.println("ERROR; Invalid command");
					}
				
					
				} catch (InvalidCommandException e) {
					out.println("ERROR;Invalid Command");
				} catch (InterruptedException e) {
					out.println("ERROR;Interrupted");
					e.printStackTrace();
				}
			}	
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.close();
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		
	}
	
	public void sendToClient(String message) {
		out.println(message);
	}
	
	public String getFromClient() throws IOException {
		return in.readLine();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}