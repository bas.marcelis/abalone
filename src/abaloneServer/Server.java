package abaloneServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
	
	private static final int PORT = 5678;
	
	private static ArrayList<ClientHandler> clients = new ArrayList<>();
	
	/**
	 * This is the main method of Server. It should be run if you want to play online using a server.
	 * It adds the clients to the server and accepts incoming traffic.
	 * @param args
	 * @throws IOException
	 */
	
	public static void main(String[] args) throws IOException {
		
		ServerSocket server = new ServerSocket(PORT);
		gameManager gamemanager = new gameManager();
		//Getting connections	
		System.out.println("[SERVER] Waiting for connection...");
		while (true) {
			Socket client = server.accept();
			//System.out.println("[SERVER] Connection made!");
			ClientHandler clientThread = new ClientHandler(client, gamemanager);
			clients.add(clientThread);
			new Thread(clientThread).start();
		}
	}
	
}