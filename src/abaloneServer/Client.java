package abaloneServer;

import abalone.Colour;
import abalone.ComputerPlayer;
import abalone.Direction;
import abalone.Field;
import abalone.Game;
import abalone.HumanPlayer;
import abalone.Move;
import abalone.Player;
import abalone.Strategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


public class Client {
	
	//	private static final int PORT = 9090;
	//	private static final String IP = "127.0.0.1";
	
	private static String gamename;
	private static Game game;
	private static String colourString;
	private static String username;
	private static int playerAmount;
	private static Colour currentColour = null;
	
	private static BufferedReader keyboard;
	private static Socket socket;
	private static BufferedReader in;
	private static PrintWriter out;
	
	private static List<Player> players = new ArrayList<>();
	
	/**
	 * Main method of class client. User has to run client and then this is method is used.
	 * @param args
	 * @throws IOException
	 */
	
	public static void main(String[] args) throws IOException {
					
		//Reader from keyboard
		keyboard = new BufferedReader(new InputStreamReader(System.in));
				
		while (true) {
			
			System.out.println("Play online or offline?");
			System.out.println("1)  Offline");
			System.out.println("2)  Online");
			System.out.println("Enter number:");
			String start = keyboard.readLine();
			if (start.contentEquals("1")) {
				playOffline();
			} else if (start.contentEquals("2")) {
				break;
			} else {
				System.out.println("Invalid value. Try again.");
			}
		}
		
		while (true) {
			try {
				System.out.println("Give IP-adress:");
				String ip = keyboard.readLine();
				
				System.out.println("Give PORT:");
				String portstring = keyboard.readLine();
				int port = Integer.parseInt(portstring);
				socket = new Socket(ip, port);
				
				System.out.println("Connecting...");
				
				//Reader from sent input from server
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				
				//Writing out to the server
				out = new PrintWriter(socket.getOutputStream(), true);
				
				break;
			} catch (Exception e) {
				System.out.println("Invalid, try again");
			}
		}
		
		//Connection has been made, client can continue
		while (true) {
			System.out.println("Enter a number to continue");
			System.out.println("1)  Find all games");
			System.out.println("2)  Create a game");
			System.out.println("3)  Join an existing game");
			System.out.println("4)  Quit");
			System.out.println(">");
			
			String command = keyboard.readLine();
			
			//Quit
			if (command.contentEquals("4")) {
				break;
			}
			
			//Find all games
			else if (command.contentEquals("1")) {
				
				out.println("FINDGAMES");
				String response = in.readLine();
				String[] gamenames = response.split(";");
				if (gamenames[0] == null || gamenames[0] == "") {
					System.out.println("No games were found");
				} else {
					for (String gamename : gamenames) {
						System.out.println(gamename);
					}
				}
				
			}
							
			//CREATING A GAME
			else if (command.contentEquals("2")) {
				while (true) {
					System.out.println("For how many players? (2/3/4)");
					command = keyboard.readLine();
					if (command.contentEquals("2") || command.contentEquals("3") || command.contentEquals("4")) {
						playerAmount = Integer.parseInt(command);
						break;
					} else {
						System.out.println("Invalid value, try again.");
					}
				}
				
				while (true) {
					System.out.println("Enter a gamename");
					command = keyboard.readLine();
					if (command != "") {
						gamename = command;
						break;
					} else {
						System.out.println("Invalid value, try again.");
					}
				}
				
				while (true) {
					System.out.println("Enter a username");
					command = keyboard.readLine();
					if (command != "") {
						username = command;
						break;
					} else {
						System.out.println("Invalid value, try again.");
					}
				}
				
				out.println("CREATEGAME;" + playerAmount + ";" + gamename + ";" + username);
				System.out.println("Game created and joined and waiting to start.");
				waitForStart();
			}
			
			//JOINING A GAME
			else if (command.contentEquals("3")) {
				while (true) {
					System.out.println("Enter the gamename of the game you want to join");
					command = keyboard.readLine();
					if (command != "") {
						gamename = command;
						break;
					} else {
						System.out.println("Invalid value, try again.");
					}
				}
				
				while (true) {
					System.out.println("Enter a username");
					command = keyboard.readLine();
					if (command != "") {
						username = command;
						break;
					} else {
						System.out.println("Invalid value, try again.");
					}
				}
				
				out.println("JOINGAME;" + gamename + ";" + username);
				System.out.println("Game joined and waiting to start...");
				waitForStart();
				
			} else {
				System.out.println("Invalid command, try again.");
			}
		}
					
		System.out.println("[SERVER] Closing...");
		socket.close();
		System.exit(0);
	}
	
	/**
	 * Wait to receive all the players.
	 * @throws IOException
	 */
	
	public static void waitForStart() throws IOException {
		
		String responseString = in.readLine();
		while (responseString == null) {
			responseString = in.readLine();
		}
		String[] response = responseString.split(";");
		
		if (response[0].contentEquals("ERROR")) {
			System.out.println("An error has occurred: " + response[1]);
		}
		
		if (response[0].contentEquals("PLAYER")) {
			
			while (true) {
				if (response[0].equals("STARTGAME")) {
					gameStart();
				} else {
					switch (response[2]) {
						case "B":
							players.clear();
							if (response[1].contentEquals(username)) {
								colourString = "B";
							}
							players.add(new Player(Colour.BLACK, response[1], null));
							break;
						case "W":
							if (response[1].contentEquals(username)) {
								colourString = "W";
							}
							players.add(new Player(Colour.WHITE, response[1], null));
							break;
						case "R":
							if (response[1].contentEquals(username)) {
								colourString = "R";
							}
							players.add(new Player(Colour.RED, response[1], null));
							break;
						case "Y":
							if (response[1].contentEquals(username)) {
								colourString = "Y";
								players.add(new Player(Colour.YELLOW, response[1], null));
								break;
							}
						default:
							break;
					}
				}
				responseString = in.readLine();
				response = responseString.split(";");
			}
		}
	}
	
	/**
	 * Starting the game.
	 * @throws IOException if there is an error with entered value.
	 */
	
	public static void gameStart() throws IOException {

		Player[] playerArray = new Player[players.size()];
		int index = 0;
		for (Player player : players) {
			playerArray[index] = player;
			index++;
		}
							
		game = new Game(gamename, playerArray);
		game.getBoard().printBoard();
		
		while (true) {
			
			String responseString;

			while (true) {
				responseString = in.readLine();	
				if (responseString != null) {
					break;
				}
			}
			
			String[] response = responseString.split(";");
			
			if (response[0].contentEquals("TURN")) {
				switch (response[1]) {
					case "B":
						currentColour = Colour.BLACK;
						break;
					case "W":
						currentColour = Colour.WHITE;
						break;
					case "R":
						currentColour = Colour.RED;
						break;
					case "Y":
						currentColour = Colour.YELLOW;
						break;
					default:
						//Unreachable
						break;
				}
			}
			
			//Enters this block if it is your turn
			if (response[0].contentEquals("TURN") && response.length > 1 && response[1].contentEquals(colourString)) {
				System.out.println("(Type leave to leave the game, type hint to get a hint, type to send a chat to all players)");
				System.out.println("[MOVE;xy;xy;Direction]    example: MOVE;00;00;NE");
				System.out.println(response[1] + "'s turn, your move. Enter move:");
				String move = keyboard.readLine();
				if (move.contentEquals("Leave") || move.contentEquals("leave")) {
					out.println("LEAVEGAME");
				} else if (move.contentEquals("Chat") || move.contentEquals("chat")) {
					System.out.println("Enter a message:");
					String message = keyboard.readLine();
					out.println("CHAT;" + message);
					System.out.println(in.readLine());
					System.out.println("Enter move:");
					move = keyboard.readLine();
					out.println(move);
				} else {
					out.println(move);
				}
			} else if (response[0].contentEquals("TURN")) {
				System.out.println(response[1] + "'s turn. Wait for your turn.");
			}
			
			//Receive a chat and print it
			if (response[0].contentEquals("CHAT")) {
				System.out.println(currentColour + ": " + response[1]);
			}
			
			//Enter a new move when the previous one is invalid
			if (response[0].contentEquals("INVALIDMOVE")) {
				System.out.println("Invalid move. Enter new move:");
				String move = keyboard.readLine();
				out.println(move);
			}
			
			//Receive move command from another player and make the move on this game
			if (response[0].contentEquals("MOVE")) {
			
				int coor1 = Integer.parseInt(response[1]);
				int coor2 = Integer.parseInt(response[2]);
				
				int y1 = coor1 % 10;
				int x1 = (coor1 / 10) % 10;
				
				int y2 = coor2 % 10;
				int x2 = (coor2 / 10) % 10;
				
				Direction direction = null;
				switch (response[3]) {
					case "NW":
						direction = Direction.NW;
						break;
					case "NE":
						direction = Direction.NE;
						break;
					case "EE":
						direction = Direction.EE;
						break;
					case "WW":
						direction = Direction.WW;
						break;
					case "SE":
						direction = Direction.SE;
						break;
					case "SW":
						direction = Direction.SW;
						break;
					default:
						break;
				}
				
				Move move = new Move(new Field(x1, y1), new Field(x2, y2), direction, currentColour);
				game.makeMove(move);
				game.getBoard().printBoard();
			}
			
			if (response[0].contentEquals("ENDGAME")) {
				
				System.out.println("Black score: " + response[2]);
				System.out.println("White score: " + response[3]);
				if (response.length > 4) {
					System.out.println("White score: " + response[4]);
				}
				if (response.length > 5) {
					System.out.println("White score: " + response[5]);
				}
				break;
			}
		}
	}
	
	/**
	 * The game is started in an online manner.
	 * It will ask multiple question to setup the game.
	 * @throws IOException
	 */
	
	public static void playOffline() throws IOException {
		String response;
		while (true) {
			System.out.println("How many players? (2/3/4)");
			response = keyboard.readLine();
			if (response.contentEquals("2") || response.contentEquals("3") || response.contentEquals("4")) {
				break;
			} else {
				System.out.println("Invalid value. Try again.");
			}
		}
		int playerAmount = Integer.parseInt(response);
		Player[] players = new Player[playerAmount];
		Colour[] colours = Colour.values();
		for (int i = 0; i < playerAmount; i++) {
			System.out.println("Enter player name (type CPU if you want a computer player");
			System.out.println("Player" + (i + 1) + ":");
			String name = keyboard.readLine();
			if (name.toUpperCase().contentEquals("CPU")) {
				players[i] = new ComputerPlayer(colours[i], "CPU" + (i + 1));
			} else {
				players[i] = new HumanPlayer(colours[i], name);
			}
		}
		Game game = new Game("NewGame", players);
		game.startGame();
	}
	
	
	
	
	
	
}