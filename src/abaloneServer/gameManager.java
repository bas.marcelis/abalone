package abaloneServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import abalone.*;
import java.util.Map;
import java.util.Set;


public class gameManager {
	
	private Map<String, GameObject> gameList = new HashMap<String, GameObject>();
	
	public gameManager() {
		
	}
	
	public void newGame(int players, String gamename, String host, ClientHandler client) throws IOException, InterruptedException {
		gameList.put(gamename, new GameObject(players, gamename, host, client));
	}
	
	public Game getGame(String gamename) {
		return gameList.get(gamename).getGame();
	}
	
	public Set<String> getAllGameNames() {
		return gameList.keySet();
	}
	
	public GameObject getGameObject(String gamename) {
		return gameList.get(gamename);
	}
	
	public boolean joinGame(String gamename, String username, ClientHandler client) throws IOException, InterruptedException {
		return gameList.get(gamename).joinGame(client, username);
	}
	
	/**
	 * This method returns a list of all the game running on the server which can be joined.
	 * @return a list of all joinable games on the server.
	 */
	
	public List<String> getJoinableGames() {
		List<String> result = new ArrayList<String>();
		for (String key : gameList.keySet()) {
			if (gameList.get(key).getGame() == null) {
				result.add(key);
			}
		}
		return result;
	}
	
}