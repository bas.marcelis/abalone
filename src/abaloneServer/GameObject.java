package abaloneServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import abalone.*;


public class GameObject {
	
	private int playerAmount;
	private String gamename;
	private String hostname;
	private List<Player> players = new ArrayList<>();
	private Game game = null;
	private Map<ClientHandler, Colour> clientToColour = new HashMap<ClientHandler, Colour>();
	
	public GameObject(int players, String gamename, String host, ClientHandler client) throws IOException, InterruptedException {
		this.playerAmount = players;
		this.gamename = gamename;
		this.hostname = host;
	}
	
	public Game getGame() {
		return game;
	}
	
	/**
	 * This method is used to chat in the game.
	 * @param message the message entered by a user which should be send.
	 */
	
	public void sendToAll(String message) {
		for (Player player : players) {
			player.getClient().sendToClient(message);
		}
	}
	
	/**
	 * The method used to join a game.
	 * @param client the client
	 * @param name the name of the game to be joined.
	 * @return true/false depending on if the game can be joined or not.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	
	public synchronized boolean joinGame(ClientHandler client, String name) throws IOException, InterruptedException {
		System.out.println(players.size());
		if (players.size() == 0 || players.size() < playerAmount) {
			if (players.size() == 0) {
				players.add(new Player(Colour.BLACK, name, client));
				sendToAll("PLAYER;" + name + ";" + players.get(0).getOneLetterColour());

			} else if (players.size() == 1) {
				players.add(new Player(Colour.WHITE, name, client));
				sendToAll("PLAYER;" + players.get(0).getName() + ";" + players.get(0).getOneLetterColour());
				sendToAll("PLAYER;" + name + ";" + players.get(1).getOneLetterColour());
				
			} else if (players.size() == 2) {
				players.add(new Player(Colour.RED, name, client));
				sendToAll("PLAYER;" + players.get(0).getName() + ";" + players.get(0).getOneLetterColour());
				sendToAll("PLAYER;" + players.get(1).getName() + ";" + players.get(1).getOneLetterColour());
				sendToAll("PLAYER;" + name + ";" + players.get(2).getOneLetterColour());
				
			} else if (players.size() == 3) {
				players.add(new Player(Colour.YELLOW, name, client));
				sendToAll("PLAYER;" + players.get(0).getName() + ";" + players.get(0).getOneLetterColour());
				sendToAll("PLAYER;" + players.get(1).getName() + ";" + players.get(1).getOneLetterColour());
				sendToAll("PLAYER;" + players.get(2).getName() + ";" + players.get(2).getOneLetterColour());
				sendToAll("PLAYER;" + name + ";" + players.get(3).getOneLetterColour());
					
			} else { 
				return false;
			}
			
			if (players.size() == playerAmount) {
				Player[] playerArray = new Player[playerAmount];
				int index = 0;
				for (Player player : players) {
					clientToColour.put(player.getClient(), player.getColour());
					playerArray[index] = player;
					index++;
				}
				this.game = new Game(gamename, playerArray);
				sendToAll("STARTGAME");
				startGame();
				notifyAll();
			} else {
				//All threads except last one need to wait.		
				wait();
			}
			
			return true;
			
		} else {
			return false;
		}
	
	}
	
	/**
	 * Method used to start the game.
	 * As long as there is no winner and less than 40 moves are made the game can be played.
	 * @throws IOException
	 */
	
	public void startGame() throws IOException {
		boolean playing = true;
		//if there are 4 players, player WHITE and player RED should be swapped to play with the correct order
		if (players.size() == 4) {
			Player temp = players.get(1);
			players.set(1, players.get(2));
			players.set(2, temp);
		}
		while (game.getWinner().size() == 0 && game.get40Moves() == null && playing) {
			for (Player player : players) {
				String colour = null;
				switch(player.getColour()) {
					case BLACK:
						colour = "B";
						break;
					case RED:
						colour = "R";
						break;
					case WHITE:
						colour = "W";
						break;
					case YELLOW:
						colour = "Y";
						break;
					default:
						break;
				}
				sendToAll("TURN;" + colour);
				String move = player.getClient().getFromClient();
				
				if (move.contentEquals("LEAVEGAME")) {
					playing = false;
					break;
				}
				
				if (move.split(";")[0].contentEquals("CHAT")) {
					sendToAll(move);
					move = player.getClient().getFromClient();
				}
				
				while (move == null || move.split(";").length < 4) {
					player.getClient().sendToClient("INVALIDMOVE");
					move = player.getClient().getFromClient();
				}
				
				while (!makeMove(move, player.getColour())) {
					player.getClient().sendToClient("INVALIDMOVE");
					move = player.getClient().getFromClient();
				}
			}
		}
		if (players.size() == 2) {
			sendToAll("ENDGAME;" + players.size() + ";" + players.get(0).getPushedMarbles() + ";" + players.get(1).getPushedMarbles());
		} else if (players.size() == 3) {
			sendToAll("ENDGAME;" + players.size() + ";" + players.get(0).getPushedMarbles() + ";" + players.get(1).getPushedMarbles() + ";" + players.get(2).getPushedMarbles());
		} else {
			sendToAll("ENDGAME;" + players.size() + ";" + players.get(0).getPushedMarbles() + ";" + players.get(1).getPushedMarbles() + ";" + players.get(2).getPushedMarbles() + ";" + players.get(3).getPushedMarbles());
		}
		
	}
	
	public boolean makeMove(String commandIn, Colour colour) {
		
		String[] command = commandIn.split(";");
		System.out.println(commandIn);
		try {
			if (command[1] == null || command[2] == null || command[3] == null) {
				throw new InvalidCommandException();
			}
			
			int coor1 = Integer.parseInt(command[1]);
			int coor2 = Integer.parseInt(command[2]);
			System.out.println(coor1 + coor2);
			int y1 = coor1 % 10;
			int x1 = (coor1/10) % 10;
			System.out.println(x1 + y1);
			int y2 = coor2 % 10;
			int x2 = (coor2/10) % 10;
			System.out.println(x2 + y2);
			Direction direction;
			switch (command[3]) {
			case "NW":
				direction = Direction.NW;
				break;
			case "NE":
				direction = Direction.NE;
				break;
			case "EE":
				direction = Direction.EE;
				break;
			case "WW":
				direction = Direction.WW;
				break;
			case "SE":
				direction = Direction.SE;
				break;
			case "SW":
				direction = Direction.SW;
				break;
			default:
				throw new InvalidCommandException();
			}
			
			Move move = new Move(new Field(x1, y1), new Field(x2, y2), direction, colour);
			System.out.println(direction);
			System.out.println(colour);
			if (game.isValidMove(move)) {
				game.makeMove(move);
				sendToAll(commandIn);
				return true;

			}
			return false;
		} catch (InvalidCommandException e) {
			return false;
		} catch (NumberFormatException e) {
			return false;
		}
		

	}
	
}