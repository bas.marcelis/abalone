package abaloneTest;
import abalone.*;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class gameTest {
	
	private Game game;
	private Player[] players;
	//game with 4 players (because it changes some game mechanics because there are teams)
	private Game game4;
	private Player[] players4;
	
	@BeforeEach
	public void setUp() {
		Player[] players = {new HumanPlayer(Colour.WHITE, "Bas"), new HumanPlayer(Colour.BLACK, "Tim")};
		this.players = players;
		this.game = new Game("NewGame", players);
		Player[] players4 = {new HumanPlayer(Colour.WHITE, "Bas"), new HumanPlayer(Colour.BLACK, "Tim"), 
							 new HumanPlayer(Colour.YELLOW, "Someone"), new HumanPlayer(Colour.RED, "Someone2")};
		this.players4 = players4;
		this.game4 = new Game("NewGameWith4", players4);
	}
	
	
	@Test
	public void makeMoveTest() {
		Field field1 = new Field(2, 2);
		Field field2 = new Field(4, 2);
		Move move1 = new Move(field1, field2, Direction.NE, Colour.WHITE);
		game.makeMove(move1);
		assertTrue(field1.getEmpty() && field2.getEmpty());
		assertFalse(game.getDirectionField(move1) == null);
		for (Field field : game.getDirectionField(move1)) {
			assertFalse(game.getBoard().getField(field).getEmpty());
			assertTrue(game.getBoard().getField(field).getMarble().getColour() == Colour.WHITE);
		}
		
		game.getBoard().getField(0, 2).addMarble(new Marble(Colour.WHITE));
		game.getBoard().getField(0, 3).addMarble(new Marble(Colour.BLACK));
		game.getBoard().getField(0, 4).addMarble(new Marble(Colour.BLACK));
		Move move2 = new Move(new Field(0, 0), new Field(0, 2), Direction.NW, Colour.WHITE);
		game.makeMove(move2);
		assertEquals(1, players[0].getPushedMarbles());
		
		Marble marbleBLACK = new Marble(Colour.BLACK);
		game.getBoard().getField(6, 2).addMarble(marbleBLACK);
		Move move3 = new Move(new Field(4, 0), new Field(5, 1), Direction.NE, Colour.WHITE);
		game.makeMove(move3);
		assertEquals(marbleBLACK, game.getBoard().getField(7, 3).getMarble());
		assertEquals(Colour.WHITE, game.getBoard().getField(6, 2).getMarble().getColour());

		
		game.getBoard().getField(7, 6).addMarble(new Marble(Colour.BLACK));
		game.getBoard().getField(8, 6).addMarble(new Marble(Colour.WHITE));
		Move move4 = new Move(new Field(7, 6), new Field(5, 6), Direction.EE, Colour.BLACK);
		game.makeMove(move4);
		assertEquals(Colour.BLACK, game.getBoard().getField(8, 6).getMarble().getColour());
		assertEquals(1, players[1].getPushedMarbles());
		
		game.getBoard().getField(3, 6).addMarble(new Marble(Colour.WHITE));
		game.getBoard().getField(4, 6).addMarble(new Marble(Colour.WHITE));
		Move move5 = new Move(new Field(5, 6), new Field(7, 6), Direction.WW, Colour.BLACK);
		game.makeMove(move5);
		assertEquals(1, players[1].getPushedMarbles());
		}
	
	@Test
	public void getDirectionFieldTest() {
		Field field1 = new Field(2, 2);
		Field field2 = new Field(4, 2);
		Move move1 = new Move(field1, field2, Direction.NE, Colour.WHITE);
		Set<Field> set = new HashSet<Field>();
		Field field11 = new Field(field1.getX()+1, field1.getY()+1);
		Field field22 = new Field(field2.getX()+1, field2.getY()+1);
		Field field33 = new Field(move1.getMiddleFieldX() + 1, move1.getMiddleFieldY() + 1);
		set.add(field11); set.add(field22); set.add(field33);
		assertEquals(set, game.getDirectionField(move1));
		
		Field field3 = new Field(3, 0);
		Field field4 = new Field(3, 2);
		Move move2 = new Move(field3, field4, Direction.NW, Colour.WHITE);
		Set<Field> set2 = new HashSet<Field>();
		set2.add(new Field(3, 3));
		assertEquals(set2, game.getDirectionField(move2));
		
		Field field5 = new Field(4, 4);
		Field field6 = new Field(0, 0);
		Move move3 = new Move(field5, field6, Direction.WW, Colour.WHITE);
		assertEquals(-1, move3.amountMarbles());
		assertNull(game.getDirectionField(move3));
		
		Set<Field> set3 = new HashSet<Field>();
		set3.add(new Field(4, 3));
		set3.add(new Field(5, 4));
		set3.add(new Field(6, 5));
		Move move4 = new Move(new Field(4, 4), new Field(6, 6), Direction.SE, Colour.WHITE);
		assertEquals(set3, game.getDirectionField(move4));
		
		Set<Field> set4 = new HashSet<Field>();
		set4.add(new Field(3, 3));
		Move move5 = new Move(new Field(4, 4), new Field(4, 4), Direction.SW, Colour.WHITE);
		assertEquals(set4, game.getDirectionField(move5));
		set4.add(new Field(3, 4));
		Move move6 = new Move(new Field(4, 4), new Field(4, 5), Direction.SW, Colour.WHITE);
		assertEquals(set4, game.getDirectionField(move6));
		set4.add(new Field(3, 5));
		Move move7 = new Move(new Field(4, 4), new Field(4, 6), Direction.SW, Colour.WHITE);
		assertEquals(set4, game.getDirectionField(move7));

		Set<Field> set5 = new HashSet<Field>();
		set5.add(new Field(3, 4));
		Move move8 = new Move(new Field(4, 4), new Field(4, 4), Direction.WW, Colour.WHITE);
		assertEquals(set5, game.getDirectionField(move8));
		set5.add(new Field(3, 5));
		Move move9 = new Move(new Field(4, 4), new Field(4, 5), Direction.WW, Colour.WHITE);
		assertEquals(set5, game.getDirectionField(move9));
		set5.add(new Field(3, 6));
		Move move10 = new Move(new Field(4, 4), new Field(4, 6), Direction.WW, Colour.WHITE);
		assertEquals(set5, game.getDirectionField(move10));
	}
	
	@Test
	public void isValidDirectionTest() {
		Field field1 = new Field(3, 0);
		Field field2 = new Field(2, 0);
		Move move1 = new Move(field1, field2, Direction.SE, Colour.WHITE);
		assertFalse(game.isValidDirection(move1));
	}
	
	@Test
	public void counterPushAmountTest() {
		for(int i = 3; i < 6; i++) {
			game.getBoard().getField(3, i).addMarble(new Marble(Colour.BLACK));
		}
		Field field = new Field(3, 2);
		Move move = new Move(field, field, Direction.NW, Colour.WHITE);
		assertEquals(3, game.counterPushAmount(move));
		game.getBoard().getField(3, 5).empty();
		assertEquals(2, game.counterPushAmount(move));
	}
	
	@Test
	public void isValidMove() {
		Field field1 = new Field(4, 0);
		Field field2 = new Field(4, 2);
		assertFalse(game.isValidMove(new Move(field1, field2, Direction.NE, Colour.WHITE)));
		assertFalse(game.isValidMove(new Move(field1, field2, Direction.SE, Colour.WHITE)));
		assertFalse(game.isValidMove(new Move(field1, field2, Direction.NW, Colour.BLACK)));
		assertTrue(game.isValidMove(new Move(field1, field2, Direction.NW, Colour.WHITE)));
		
		game.getBoard().getField(4, 3).addMarble(new Marble(Colour.BLACK));
		game.getBoard().getField(4, 4).addMarble(new Marble(Colour.BLACK));
		assertTrue(game.isValidMove(new Move(field1, field2, Direction.NW, Colour.WHITE)));
		game.getBoard().getField(4, 5).addMarble(new Marble(Colour.BLACK));
		assertFalse(game.isValidMove(new Move(field1, field2, Direction.NW, Colour.WHITE)));
		assertFalse(game.isValidMove(new Move(field1, new Field(4, 4), Direction.NW, Colour.WHITE)));

		
		Field field3 = new Field(2, 2);
		assertFalse(game.isValidMove(new Move(field3, field2, Direction.NE, Colour.WHITE)));
		assertFalse(game.isValidMove(new Move(field3, field2, Direction.NW, Colour.WHITE)));
		assertFalse(game.isValidMove(new Move(field3, field1, Direction.EE, Colour.WHITE)));
		
		Field field4 = new Field(-1, 0);
		assertFalse(game.isValidMove(new Move(field4, field4, Direction.NW, Colour.WHITE)));
		
		//Clear the whole board
		Set<Integer> keySet = game.getBoard().getBoardMap().keySet();
		for (Integer integer : keySet) {
			game.getBoard().getField(game.getBoard().getBoardMap().get(integer)).empty();
		}
		
		game.getBoard().getField(0, 0).addMarble(new Marble(Colour.WHITE));
		game.getBoard().getField(0, 1).addMarble(new Marble(Colour.BLACK));
		assertFalse(game.isValidMove(new Move(new Field(0, 0), new Field(0, 1), Direction.NW, Colour.BLACK)));
		game.getBoard().getField(0, 1).empty();
		game.getBoard().getField(0, 1).addMarble(new Marble(Colour.RED));
		assertFalse(game.isValidMove(new Move(new Field(0, 0), new Field(0, 1), Direction.NW, Colour.WHITE)));
		
		//Clear the whole board
		for (Integer integer : keySet) {
			game.getBoard().getField(game.getBoard().getBoardMap().get(integer)).empty();
		}
		
		game4.getBoard().getField(0, 0).addMarble(new Marble(Colour.WHITE));
		game4.getBoard().getField(0, 1).addMarble(new Marble(Colour.BLACK));
		assertFalse(game4.isValidMove(new Move(new Field(0, 0), new Field(0, 1), Direction.NW, Colour.BLACK)));

	}
	
	@Test
	public void getWinnerTest() {
		for (int i = 0; i < 6; i++) {
			//Clear the whole board
			Set<Integer> keySet = game.getBoard().getBoardMap().keySet();
			for (Integer integer : keySet) {
				game.getBoard().getField(game.getBoard().getBoardMap().get(integer)).empty();
			}
			game.getBoard().getField(6, 6).addMarble(new Marble(Colour.WHITE));
			game.getBoard().getField(7, 7).addMarble(new Marble(Colour.WHITE));
			game.getBoard().getField(8, 8).addMarble(new Marble(Colour.BLACK));
			Move move = new Move(new Field(6, 6), new Field(7, 7), Direction.NE, Colour.WHITE);
			game.makeMove(move);
		}
		Set<Player> winner = new HashSet<Player>();
		winner.add(game.getColourMap().get(Colour.WHITE));
		assertEquals(game.getWinner(), winner);	
	}
	
	@Test
	public void getWinnerTest4Players1() {
		
		assertEquals(4, game4.getPlayerAmount());
		
		for (int i = 0; i < 3; i++) {
			//Clear the whole board
			Set<Integer> keySet = game4.getBoard().getBoardMap().keySet();
			for (Integer integer : keySet) {
				game4.getBoard().getField(game4.getBoard().getBoardMap().get(integer)).empty();
			}
			game4.getBoard().getField(6, 6).addMarble(new Marble(Colour.WHITE));
			game4.getBoard().getField(7, 7).addMarble(new Marble(Colour.WHITE));
			game4.getBoard().getField(8, 8).addMarble(new Marble(Colour.RED));
			Move move = new Move(new Field(6, 6), new Field(7, 7), Direction.NE, Colour.WHITE);
			game4.makeMove(move);
		}
		for (int i = 0; i < 3; i++) {
			//Clear the whole board
			Set<Integer> keySet = game4.getBoard().getBoardMap().keySet();
			for (Integer integer : keySet) {
				game4.getBoard().getField(game4.getBoard().getBoardMap().get(integer)).empty();
			}
			game4.getBoard().getField(6, 6).addMarble(new Marble(Colour.BLACK));
			game4.getBoard().getField(7, 7).addMarble(new Marble(Colour.BLACK));
			game4.getBoard().getField(8, 8).addMarble(new Marble(Colour.YELLOW));
			Move move = new Move(new Field(6, 6), new Field(7, 7), Direction.NE, Colour.BLACK);
			game4.makeMove(move);
		}
		
		Set<Player> winner = new HashSet<Player>();
		winner.add(game4.getColourMap().get(Colour.WHITE));
		winner.add(game4.getColourMap().get(Colour.BLACK));
		assertEquals(game4.getWinner(), winner);
	}
	
	@Test
	public void getWinnerTest4Players2() {
		for (int i = 0; i < 3; i++) {
			//Clear the whole board
			Set<Integer> keySet = game4.getBoard().getBoardMap().keySet();
			for (Integer integer : keySet) {
				game4.getBoard().getField(game4.getBoard().getBoardMap().get(integer)).empty();
			}
			game4.getBoard().getField(6, 6).addMarble(new Marble(Colour.RED));
			game4.getBoard().getField(7, 7).addMarble(new Marble(Colour.RED));
			game4.getBoard().getField(8, 8).addMarble(new Marble(Colour.WHITE));
			Move move = new Move(new Field(6, 6), new Field(7, 7), Direction.NE, Colour.RED);
			game4.makeMove(move);
		}
		for (int i = 0; i < 3; i++) {
			//Clear the whole board
			Set<Integer> keySet = game4.getBoard().getBoardMap().keySet();
			for (Integer integer : keySet) {
				game4.getBoard().getField(game4.getBoard().getBoardMap().get(integer)).empty();
			}
			game4.getBoard().getField(6, 6).addMarble(new Marble(Colour.YELLOW));
			game4.getBoard().getField(7, 7).addMarble(new Marble(Colour.YELLOW));
			game4.getBoard().getField(8, 8).addMarble(new Marble(Colour.BLACK));
			Move move = new Move(new Field(6, 6), new Field(7, 7), Direction.NE, Colour.YELLOW);
			game4.makeMove(move);
		}
		
		Set<Player> winner2 = new HashSet<Player>();
		winner2.add(game4.getColourMap().get(Colour.RED));
		winner2.add(game4.getColourMap().get(Colour.YELLOW));
		assertEquals(game4.getWinner(), winner2);
	}
	
	@Test
	public void get40MovesTest() {
		//Checks for a draw
		Game gameCopy = game.deepCopy();
		for (int i = 1; i < 20; i++) {
			game.makeMove(new Move(new Field(0,1), new Field(0,1), Direction.NW, Colour.WHITE));
			game.makeMove(new Move(new Field(0,2), new Field(0,2), Direction.SE, Colour.WHITE));
		}
		game.makeMove(new Move(new Field(0,1), new Field(0,1), Direction.NW, Colour.WHITE));
		assertNull(game.get40Moves());
		game.makeMove(new Move(new Field(0,2), new Field(0,2), Direction.SE, Colour.WHITE));
		assertEquals(game.get40Moves(), "It's a draw!");
		
		game = gameCopy.deepCopy();
		game.getBoard().getField(0, 4).addMarble(new Marble(Colour.WHITE));
		game.getBoard().getField(1, 4).addMarble(new Marble(Colour.BLACK));
		game.getBoard().getField(2, 4).addMarble(new Marble(Colour.BLACK));
		game.makeMove(new Move(new Field(1,4), new Field(2,4), Direction.WW, Colour.BLACK));
		for (int i = 1; i < 20; i++) {
			game.makeMove(new Move(new Field(3,7), new Field(3,7), Direction.SW, Colour.BLACK));
			game.makeMove(new Move(new Field(2,6), new Field(2,6), Direction.NE, Colour.BLACK));
		}
		assertNull(game.get40Moves());
		game.makeMove(new Move(new Field(3,7), new Field(3,7), Direction.SW, Colour.BLACK));
		assertEquals(game.get40Moves(), "Tim won!");
		
		game = gameCopy.deepCopy();

	}
	
	
	
	
	
	
	
	
	
	
	
}