package abaloneTest;

import abalone.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import abalone.Colour;

public class StrategyTest {
	
	private Game game;
	private Strategy strategy;
	private Game game2;
	private Strategy strategy2;
	
	@BeforeEach
	public void setUp() {
		Player cpu = new ComputerPlayer(Colour.WHITE, "cpu");
		Player[] players = {cpu, new HumanPlayer(Colour.BLACK, "Someone")};
		this.game = new Game("NewGame", players);
		this.strategy = new Strategy(Colour.WHITE);
		
		Player cpu2 = new ComputerPlayer(Colour.BLACK, "cpu");
		Player[] players2 = {cpu, new HumanPlayer(Colour.WHITE, "Someone")};
		this.game2 = new Game("NewGame", players2);
		this.strategy2 = new Strategy(Colour.BLACK);
	}
	
	@Test
	public void determineMoveStartWhiteTest() {
		Move move1 = strategy.determineMove(game);
		assertEquals(move1.getField1(), new Field(2, 2));
		assertEquals(move1.getField2(), new Field(4, 2));
		assertEquals(move1.getDirection(), Direction.NE);
		
		Move move2 = strategy.determineMove(game);
		assertEquals(move2.getField1(), new Field(1, 1));
		assertEquals(move2.getField2(), new Field(3, 1));
		assertEquals(move2.getDirection(), Direction.NE);
		
		Move move3 = strategy.determineMove(game);
		assertEquals(move3.getField1(), new Field(0, 0));
		assertEquals(move3.getField2(), new Field(2, 0));
		assertEquals(move3.getDirection(), Direction.NE);
		
		Move move4 = strategy.determineMove(game);
		assertEquals(move4.getField1(), new Field(3, 0));
		assertEquals(move4.getField2(), new Field(4, 1));
		assertEquals(move4.getDirection(), Direction.NE);
		
		Move move5 = strategy.determineMove(game);
		assertEquals(move5.getField1(), new Field(2, 1));
		assertEquals(move5.getField2(), new Field(2, 2));
		assertEquals(move5.getDirection(), Direction.NW);
	}
	
	@Test
	public void determineMoveStartBlackTest() {
		game = game2;
		strategy = strategy2;
		Move move1 = strategy.determineMove(game);
		assertEquals(move1.getField1(), new Field(4, 6));
		assertEquals(move1.getField2(), new Field(6, 6));
		assertEquals(move1.getDirection(), Direction.SW);
		
		Move move2 = strategy.determineMove(game);
		assertEquals(move2.getField1(), new Field(5, 7));
		assertEquals(move2.getField2(), new Field(7, 7));
		assertEquals(move2.getDirection(), Direction.SW);
		
		Move move3 = strategy.determineMove(game);
		assertEquals(move3.getField1(), new Field(6, 8));
		assertEquals(move3.getField2(), new Field(8, 8));
		assertEquals(move3.getDirection(), Direction.SW);
		
		Move move4 = strategy.determineMove(game);
		assertEquals(move4.getField1(), new Field(5, 8));
		assertEquals(move4.getField2(), new Field(4, 7));
		assertEquals(move4.getDirection(), Direction.SW);
		
		Move move5 = strategy.determineMove(game);
		assertEquals(move5.getField1(), new Field(6, 6));
		assertEquals(move5.getField2(), new Field(6, 7));
		assertEquals(move5.getDirection(), Direction.SE);
	}
	
	@Test
	public void determineMoveTest() {
		//Cancels out the 5 standard start moves
		for (int i = 0; i < 5; i++) {
			Move move = strategy.determineMove(game);
			game.makeMove(move);
		}
		game.getBoard().getField(6, 2).addMarble(new Marble(Colour.BLACK));
		Move move2 = strategy.determineMove(game);
		assertEquals(move2.getDirection(), Direction.EE);
		assertTrue((move2.getField1().getX() == 5 && move2.getField1().getY() == 2) || 
				(move2.getField2().getX() == 5 && move2.getField2().getY() == 2));
		game.makeMove(move2);
		Move move3 = strategy.determineMove(game);
		game.makeMove(move3);
	}
	
	@Test
	public void test3PlayerGame() {
		Player[] players = {new HumanPlayer(Colour.WHITE, "Bas"), new HumanPlayer(Colour.BLACK, "Bas"), new HumanPlayer(Colour.RED, "Bas")};
		game = new Game("NewGame", players);
		strategy.determineMove(game);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}