package abaloneTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import abalone.*;

public class MoveTest {
	
	private Game game;
	
	@BeforeEach
	public void setUp() {
		Player[] players = {new HumanPlayer(Colour.WHITE, "Bas"), new HumanPlayer(Colour.BLACK, "Tim")};
		this.game = new Game("NewGame", players);
	}
	
	
	@Test
	public void generalTest() {
		Field field1 = new Field(0, 0);
		Field field2 = new Field(1, 1);
		Field field3 = new Field(2, 2);
		Move move2 = new Move(field1, field2, Direction.NW, Colour.WHITE);
		
		assertEquals(field1, move2.getField1());
		assertEquals(field2, move2.getField2());
		assertEquals(Colour.WHITE, move2.getColour());
		assertEquals(Direction.NW, move2.getDirection());
		
		Move move3 = new Move(field1, field3, Direction.NW, Colour.WHITE);
		assertEquals(1, move3.getMiddleFieldX());
		assertEquals(1, move3.getMiddleFieldY());
		assertEquals(field2, move3.getMiddleField());
	}
	
	@Test
	public void getTeamColourTest() {
		Field field1 = new Field(0, 0);
		Move move1 = new Move(field1, field1, Direction.NW, Colour.WHITE);
		Move move2 = new Move(field1, field1, Direction.NW, Colour.BLACK);
		Move move3 = new Move(field1, field1, Direction.NW, Colour.RED);
		Move move4 = new Move(field1, field1, Direction.NW, Colour.YELLOW);
		assertEquals(Colour.BLACK, move1.getTeamColour(4));
		assertEquals(Colour.WHITE, move2.getTeamColour(4));
		assertEquals(Colour.YELLOW, move3.getTeamColour(4));
		assertEquals(Colour.RED, move4.getTeamColour(4));
		assertEquals(Colour.WHITE, move1.getTeamColour(3));
	}
	
	@Test
	public void amountMarblesTest() {
		Field field1 = new Field(0, 0);
		Field field2 = new Field(1, 1);
		Field field3 = new Field(2, 2);
		Field field4 = new Field(3, 3);
		Move move1 = new Move(field1, field1, Direction.NW, Colour.WHITE);	
		assertEquals(1, move1.amountMarbles());
		Move move2 = new Move(field1, field2, Direction.NW, Colour.WHITE);
		assertEquals(2, move2.amountMarbles());
		Move move3 = new Move(field1, field3, Direction.NW, Colour.WHITE);
		assertEquals(3, move3.amountMarbles());
		Move move4 = new Move(field1, field4, Direction.NW, Colour.WHITE);
		assertEquals(-1, move4.amountMarbles());
		Field field5 = new Field(4, 4);
		Field field6 = new Field(5, 6);
		Move move5 = new Move(field5, field6, Direction.NW, Colour.WHITE);
		assertEquals(-1, move5.amountMarbles());
	}
	
	@Test
	public void getFrontTest() {
		Field field1 = new Field(4, 4);
		Field field2 = new Field(5, 5);
		Field field3 = new Field(4, 6);
		Field field4 = new Field(4, 5);
		
		assertEquals(field1, new Move(field1, field3, Direction.SE, Colour.WHITE).getFront());
		assertEquals(field1, new Move(field3, field1, Direction.SE, Colour.WHITE).getFront());
		assertNull(new Move(field4, field2, Direction.SE, Colour.WHITE).getFront());

		assertEquals(field3, new Move(field1, field3, Direction.NW, Colour.WHITE).getFront());
		assertEquals(field3, new Move(field3, field1, Direction.NW, Colour.WHITE).getFront());
		assertNull(new Move(field4, field2, Direction.NW, Colour.WHITE).getFront());

		assertEquals(field2, new Move(field1, field2, Direction.NE, Colour.WHITE).getFront());
		assertEquals(field2, new Move(field2, field1, Direction.NE, Colour.WHITE).getFront());
		assertNull(new Move(field4, field2, Direction.NE, Colour.WHITE).getFront());
	
		assertEquals(field2, new Move(field4, field2, Direction.EE, Colour.WHITE).getFront());
		assertEquals(field2, new Move(field2, field4, Direction.EE, Colour.WHITE).getFront());
		assertNull(new Move(field1, field3, Direction.EE, Colour.WHITE).getFront());
		
		assertEquals(field4, new Move(field4, field2, Direction.WW, Colour.WHITE).getFront());
		assertEquals(field4, new Move(field2, field4, Direction.WW, Colour.WHITE).getFront());
		assertNull(new Move(field1, field3, Direction.WW, Colour.WHITE).getFront());
		
		assertEquals(field1, new Move(field1, field2, Direction.SW, Colour.WHITE).getFront());
		assertEquals(field1, new Move(field2, field1, Direction.SW, Colour.WHITE).getFront());
		assertNull(new Move(field4, field2, Direction.SW, Colour.WHITE).getFront());
	}
	
	@Test
	public void getBackTest() {
		Field field1 = new Field(4, 4);
		assertEquals(field1, new Move(field1, field1, Direction.NW, Colour.WHITE).getBack());
		Field field2 = new Field(5, 5);
		assertEquals(field1, new Move(field1, field2, Direction.NE, Colour.WHITE).getBack());
		Field field3 = new Field(4, 6);
		assertEquals(field3, new Move(field1, field3, Direction.SE, Colour.WHITE).getBack());
		assertNull(new Move(field1, field3, Direction.EE, Colour.WHITE).getBack());
	}
	
}