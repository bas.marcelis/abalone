package abalone;

import java.util.HashSet;
import java.util.Set;

/**
 * All the coordinates in the board are saved here.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 */

public class Field {
	
	private int x;
	private int y;
	private Marble marble = null;
	
	/**
	 * Get the x and y coordinate of a field.
	 * @param x coordinate on the board
	 * @param y coordinate on the board
	 * @ensures 0<x<=8
	 * @ensures 0<y<=8
	 */
	
	public Field(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Return a deepcopy of the field used in strategy.
	 * @return the deepcopy of field
	 */
	
	public Field deepCopy() {
		Field field = new Field(x, y);
		if (marble != null) {
			field.addMarble(marble.deepCopy());
		}
		return field;
	}
	
	//Automatically created method for tests
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marble == null) ? 0 : marble.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	//Automatically created method for tests
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Field other = (Field) obj;
		if (marble == null) {
			if (other.marble != null) {
				return false;
			}
		} else if (!marble.equals(other.marble)) {
			return false;
		}
		if (x != other.x) {
			return false;
		}
		if (y != other.y) {
			return false;
		}
		return true;
	}

	/**
	 * There is no marble at this field.
	 */
	
	public void empty() {
		//this.empty = true;
		marble = null;
	}
	
	/**
	 * Check if a field is empty or not.
	 * @return true if there is no marble in this field
	 */
	
	public boolean getEmpty() {
		return (marble == null);
	}
	/**
	 * Get the x coordinate of the field. 
	 * @return the x coordinate
	 * @ensures 0<x<=8
	 */
	
	public int getX() {
		return x;
	}
	
	/**
	 * Get the y coordinate of the field.
	 * @return the y coordinate
	 * @ensures 0<y<=8
	 */
	
	public int getY() {
		return y;
	}
	
	/**
	 * To be implemented.
	 * @param marble to be implemented
	 */
	
	public void addMarble(Marble marble) {
		this.marble = marble;
		//empty = false;
	}
	
	/**
	 * Get the marble of this field.
	 * @return the marble on this field
	 */
	
	public Marble getMarble() {
		return marble;
	}
	
	/**
	 * This method will give the appropriate letter to a field.
	 * If a field is not occupied by a marble this will be "O".
	 *@return will return "O" which is the placeholder for an empty field or returns the colour of the marble.
	 */
	
	public String toString() {
		if (this.marble == null || this.marble.getColour() == null) {
			return "O";
		} else {
			return marble.toString();
		}
	}
	

}