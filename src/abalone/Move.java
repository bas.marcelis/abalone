package abalone;

import java.util.Set;

/**
 * The move class. It has several methods that are used when validating or making moves.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */
public class Move {
	
	private Field field1;
	private Field field2;
	private Direction direction;
	private Colour colour;
	
	/**
	 * The constructor of move.
	 * @param field1 first selected marble.
	 * @param field2 second selected marble.
	 * @param direction the direction the move goes.
	 * @param colour the colour of the player making the move.
	 */
	
	public Move(Field field1, Field field2, Direction direction, Colour colour) {
		this.field1 = field1;
		this.field2 = field2;
		this.direction = direction;
		this.colour = colour;
	}
	
	public Field getField1() {
		return field1;
	}
	
	public Field getField2() {
		return field2;
	}
	
	/**
	 * This method uses the x of the 2 marbles to calculate the value in between. 
	 * @return the x coordinate of the field if three marble are to be moved.
	 */
	
	public int getMiddleFieldX() {
		int x = field1.getX();
		int x2 = field2.getX();
		return ((x + x2) / 2);
	}
	
	/**
	 * This method uses the y of the 2 marbles to calculate the value in between. 
	 * @return the y coordinate of the field if three marble are to be moved.
	 */
	
	public int getMiddleFieldY() {
		int y = field1.getY();
		int y2 = field2.getY();
		return ((y + y2) / 2);
	}
	
	public Field getMiddleField() {
		return new Field(getMiddleFieldX(), getMiddleFieldY());
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	public Colour getColour() {
		return colour;
	}
	
	/**
	 * This method uses your colour and returns you the colour of your teammate.
	 * @param amountPlayers the amount of players playing this game.
	 * @return the colour of your teammate.
	 */
	
	public Colour getTeamColour(int amountPlayers) {
		
		if (amountPlayers == 4) {
			switch (colour) {
				case BLACK:
					return Colour.WHITE;
				case RED:
					return Colour.YELLOW;
				case WHITE:
					return Colour.BLACK;
				case YELLOW:
					return Colour.RED;
				default:
					break;
			}
		}
		return colour;
	}
	
	
	/**
	 * This method calculates how many marbles you selected.
	 * @return the amount of selected marbles.
	 */
	
	public int amountMarbles() {
		int x1 = field1.getX();
		int y1 = field1.getY();
		int x2 = field2.getX();
		int y2 = field2.getY();
		double x3 = (x1 + x2) / (double) 2;
		double y3 = (y1 + y2) / (double) 2;
		
		if (x1 == x2 && y1 == y2) {
			return 1;
		}
		
		if (Math.abs(x1 - x2) > 2 || Math.abs(y1 - y2) > 2) {
			return -1;
		}
		
		if (x3 % 1 == 0 && y3 % 1 == 0) {
			return 3;
		}
		
		if ((Math.abs(x1 - x2) == 1 && y1 == y2) || (Math.abs(y1 - y2) == 1 && x1 == x2) || (x1 - x2 == y1 - y2)) {
			return 2;
		}
		
		return -1;
	}
	
	/**
	 * This method will give you the marble in the front of the selection.
	 * It gives null if it moves sideways because there is no front marble.
	 * @return the marble in the front of the selection or null if it moves sideways.
	 */
	
	public Field getFront() {
		int x1 = field1.getX();
		int y1 = field1.getY();
		int x2 = field2.getX();
		int y2 = field2.getY();
		
		if (field1.getX() == field2.getX() && field1.getY() == field2.getY()) {
			return field1;
		}
		
		switch (direction) {
			case EE:
				if (y1 == y2) {
					if (x1 > x2) {
						return field1;
					} else {
						return field2;
					}
				} else {
					return null;
				}
			case NE:
				if (x1 == x2 || y1 == y2) {
					return null;
				} else if (x1 > x2) {
					return field1;
				} else {
					return field2;
				}
			case NW:
				if (x1 == x2) {
					if (y2 > y1) {
						return field2;
					} else {
						return field1;
					}
				} else {
					return null;
				}
			case SE:
				if (x1 == x2) {
					if (y2 > y1) {
						return field1;
					} else {
						return field2;
					}
				} else {
					return null;
				}
			case SW:
				if (x1 == x2 || y1 == y2) {
					return null;
				} else if (x1 < x2) {
					return field1;
				} else {
					return field2;
				}
			case WW:
				if (y1 == y2) {
					if (x1 > x2) {
						return field2;
					} else {
						return field1;
					}
				} else {
					return null;
				}
			default:
				return null;

		}
	}
	
	/**
	 * This method will give you the marble in the back of the selection.
	 * It returns null if it moves sideways.
	 * It uses getFront and get the opposite marble.
	 * @return the marble in the back of the selection.
	 */
	
	public Field getBack() {
		if (field1 == getFront()) {
			return field2;
		} else if (field2 == getFront()) {
			return field1;
		} else {
			return null;
		}
	}
}