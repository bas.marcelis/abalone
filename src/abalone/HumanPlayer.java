package abalone;

import java.util.Scanner;

/**
 * This is the class the player will use to play the game by making moves.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */

public class HumanPlayer extends Player {
	
	public HumanPlayer(Colour colour, String name) {
		super(colour, name, null);
	}
	
	/**
	 * The player can give the command to make a move.
	 * It selects two coordinates.
	 * Still comments to be implemented.
	 * @param field1 the first coordinate selected
	 * @param field2 the second coordinate selected
	 * @return coordinates of field1, field2, the desired direction and the colour of the marble.
	 * @requires field1 to be written in the format x, y
	 * @requires field2 to be written in the format x, y
	 */
	@Override
	public Move makeMove(Game game) {
		System.out.println("Human Player Move");
		Scanner scanner = new Scanner(System.in);
		System.out.println(colour.toString() + "'s turn");
		System.out.println("Enter move:");
		Field field1 = null;
		Field field2 = null;
		
		/* 
		 * Asks the player to enter their desired first marble to be selected.
		 */
		while (field1 == null) {
			System.out.println("First marble: (x, y)");
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				if (line != null && !line.isBlank()) {
					String[] field1string = line.split(", ");
					field1 = new Field(Integer.valueOf(field1string[0]), Integer.valueOf(field1string[1]));
					break;
				}
			}
		}
		
		/*
		 * Asks the player to enter their desired second marble to be selected. 
		 * If the player want to select only one marble he should and the same marble as his/her second marble.
		 */
		while (field2 == null) {
			System.out.println("Second marble: (x, y)");
			while (scanner.hasNext()) {
				String line2 = scanner.nextLine();
				if (line2 != null) {
					String[] field2string = line2.split(", ");
					field2 = new Field(Integer.valueOf(field2string[0]), Integer.valueOf(field2string[1]));
					break;
				}
			}
		}
		/*
		 * After the player has selected the desired marble(s) he/she can choose a direction.
		 */
		Direction direction = null;
		while (direction == null) {
			System.out.println("Direction: (NW, NE, EE, WW, SE, SW)");
			
			String directionstring = null;
			while (scanner.hasNext()) {
				directionstring = scanner.nextLine();
				if (directionstring != null) {
					break;
				}
			}
			
			switch (directionstring) {
				case "NW" :
					direction = Direction.NW;
					break;
				case "NE" :
					direction = Direction.NE;
					break;
				case "EE" :
					direction = Direction.EE;
					break;
				case "WW" :
					direction = Direction.WW;
					break;
				case "SE" :
					direction = Direction.SE;
					break;
				case "SW" :
					direction = Direction.SW;
					break;
				default:
					break;
			}
			if (direction == null) {
				System.out.println("Not a valid direction");
			}
		}
		//Gather all the information gained in this method and return this.
		return new Move(field1, field2, direction, colour);
	}
	
}