package abalone;

/**
 * To be implemented.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */

public class ComputerPlayer extends Player {
	
	private Strategy strategy;
	
	public ComputerPlayer(Colour colour, String name) {
		super(colour, name, null);
		this.strategy = new Strategy(colour);
	}
	
	/**
	 * let the CPU make a move using the strategy we designed.
	 */
	
	public Move makeMove(Game game) {
		System.out.println("CPU move:");
		return strategy.determineMove(game);
	}
	
}