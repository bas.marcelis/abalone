package abalone;

import java.util.HashMap;
import java.util.Set;

/**
 * Control the board by printing it to the console.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */

public class Board {
	
	private HashMap<Integer, Field> fieldMap = new HashMap<Integer, Field>();
	
	/**
	 * Set up the gameboard with the amount of players playing.
	 * This amount is given by the class game
	 * @param players amount of players is given by class game
	 */
	
	public Board(int players) {
		makeBoard();
		switch (players) {
			case 2:
				setUp2();
				break;
			case 3:
				setUp3();
				break;
			case 4:
				setUp4();
				break;
			default:
				System.out.println("Error with switch Board. Unsuspected value");
				break;
		}
	}
	
	/**
	 * Create all fields. Puts it in a map with the coordinates as a key.
	 */
	
	public HashMap<Integer, Field> getBoardMap() {
		return fieldMap;
	}
	
	/**
	 * Get a deepCopy of the board. 
	 * This is used in strategy because it needs a copy of the original board and test out new moves on it.
	 * And when it has determined a move it should make the move on the old version of the board.
	 * @return A map of all the fields in the board.
	 */
	
	public Board deepCopy() {
		Board newBoard = new Board(2);
		Set<Integer> keySet = fieldMap.keySet();
		HashMap<Integer, Field> newMap = new HashMap<Integer, Field>();
		for (Integer integer : keySet) {
			newMap.put(integer, fieldMap.get(integer).deepCopy());
		}
		newBoard.fieldMap = newMap;
		return newBoard;
	}
	
	/**
	 * Create the fields and put them in a map. 
	 * Convert the 2 coordinates into one number in the format YX.	 * 
	 */
	
	public void makeBoard() {
		for (int x = 0, y = 0; x < 5; x++) {
			int converter = 10 * y + x;	//Convert the 2 coordinates to one nmber YX
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 0, y = 1; x < 6; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 0, y = 2; x < 7; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 0, y = 3; x < 8; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 0, y = 4; x < 9; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 1, y = 5; x < 9; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 2, y = 6; x < 9; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 3, y = 7; x < 9; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
		for (int x = 4, y = 8; x < 9; x++) {
			int converter = 10 * y + x;
			fieldMap.put(converter, new Field(x, y));
		}
	}
	/**
	 * Convert the 2 coordinates to 1 number so you don't need 2 integers.
	 * All coordinates are turned to 1 integer.
	 * @requires x>=0 && x<=8
	 * @requires y>=0 && y<=8
	 * @param x coordinate of the place on the board
	 * @param y coordinate of the place on the board
	 * @return
	 */
	
	public Field getField(int x, int y) {
		int converter = 10 * y + x;
		return fieldMap.get(converter);
	}
	
	/**
	 * Get the x and y values of the requested field.
	 * @param field enter the field you want to know the x and y coordinates of
	 * @return the x and y coordinates of the entered field.
	 */
	
	public Field getField(Field field) {
		return getField(field.getX(), field.getY());
	}
	
	/**
	 * First converts the x,y coordinates to one integer.
	 * Then creates the map for the valid fields.
	 * @requires x>=0 && x<=8
	 * @requires y>=0 && y<=8
	 * @param x coordinate of the place on the board
	 * @param y coordinate of the place on the board
	 * @return
	 */
	
	public boolean isField(int x, int y) {
		int converter = 10 * y + x;
		Set<Integer> keys = fieldMap.keySet();
		return (keys.contains(converter));
	}
	
	/**
	 * Enter a field and see if it is a field on the board.
	 * @param field the requested field to see if it is on the board.
	 * @return true or false depending on if the field exists on the board or not.
	 */
	
	public boolean isField(Field field) {
		return isField(field.getX(), field.getY());
	}
	
	/**
	 * Setting up the game for 2 players.
	 * It uses 2 players with 14 marbles each
	 * When 2 players play the game they have the colours WHITE and BLACK.
	 * It uses getField to get the correct fields and addMarble to add new marbles with the colour of the player.
	 */
	
	public void setUp2() {
		// Creating the black and white set ups
		for (int x = 0; x < 5; x++) {
			getField(x + 4, 8).addMarble(new Marble(Colour.BLACK));
			getField(x, 0).addMarble(new Marble(Colour.WHITE));
			getField(x + 3, 7).addMarble(new Marble(Colour.BLACK));
			getField(x, 1).addMarble(new Marble(Colour.WHITE));
		}
		getField(8, 7).addMarble(new Marble(Colour.BLACK));
		getField(5, 1).addMarble(new Marble(Colour.WHITE));
		for (int x = 2; x < 5; x++) {
			getField(x, 2).addMarble(new Marble(Colour.WHITE));
			getField(x + 2, 6).addMarble(new Marble(Colour.BLACK));
		}
	}
	
	/**
	 * Creating the board for 3 players with colours BLACK WHITE and RED.	 * 
	 * It uses getField to get the correct fields and addMarble to add new marbles with the colour of the player.
	 * Each player has 11 marbles.
	 */
	
	public void setUp3() {
		//Creating white part
		for (int y = 4, x = 0; y < 9; y++, x++) {
			getField(x, y).addMarble(new Marble(Colour.WHITE));
			getField(x + 1, y).addMarble(new Marble(Colour.WHITE));
		}
		getField(0, 3).addMarble(new Marble(Colour.WHITE));
		
		//Creating black part
		for (int y = 4, x = 7; y < 9; y++) {
			getField(x, y).addMarble(new Marble(Colour.BLACK));
			getField(x + 1, y).addMarble(new Marble(Colour.BLACK));
		}
		getField(7, 3).addMarble(new Marble(Colour.BLACK));
		
		//Creating red part
		for (int  x = 0; x < 5;  x++) {
			getField(x, 0).addMarble(new Marble(Colour.RED));
			getField(x, 1).addMarble(new Marble(Colour.RED));
		}
		getField(5, 1).addMarble(new Marble(Colour.RED));
	}
	
	/**
	 * Creating the board for 4 players with the colours black white red and yellow.
	 * It uses getField to get the correct fields and addMarble to add new marbles with the colour of the player.
	 * Each player has 9 marbles
	 */
	
	public void setUp4() {
		//Creating white part
		for (int y = 1; y < 5; y++) {
			getField(0, y).addMarble(new Marble(Colour.WHITE));
		}
		for (int y = 2; y < 5; y++) {
			getField(1, y).addMarble(new Marble(Colour.WHITE));
		}
		getField(2, 3).addMarble(new Marble(Colour.WHITE));
		getField(2, 4).addMarble(new Marble(Colour.WHITE));
		
		//Creating black part
		for (int y = 4; y < 8; y++) {
			getField(8, y).addMarble(new Marble(Colour.BLACK));
		}
		for (int y = 4; y < 7; y++) {
			getField(7, y).addMarble(new Marble(Colour.BLACK));
		}
		getField(6, 4).addMarble(new Marble(Colour.BLACK));
		getField(6, 5).addMarble(new Marble(Colour.BLACK));
		
		//Creating red part
		for (int x = 1; x < 5; x++) {
			getField(x, 0).addMarble(new Marble(Colour.RED));
		}
		for (int x = 2; x < 5; x++) {
			getField(x, 1).addMarble(new Marble(Colour.RED));
		}
		getField(3, 2).addMarble(new Marble(Colour.RED));
		getField(4, 2).addMarble(new Marble(Colour.RED));
		
		//Creating yellow part
		for (int x = 4; x < 8; x++) {
			getField(x, 8).addMarble(new Marble(Colour.YELLOW));
		}
		for (int x = 4; x < 7; x++) {
			getField(x, 7).addMarble(new Marble(Colour.YELLOW));
		}
		getField(4, 6).addMarble(new Marble(Colour.YELLOW));
		getField(5, 6).addMarble(new Marble(Colour.YELLOW));
	}
	/**
	 * Actually printing the coordinates to the console.
	 * This is used by the player to easily figure out what the coordinates are for a marble.
	 */
	
	public void printCoorBoard() {
		System.out.println("---------------/ 4,8 / 5,8 / 6,8 \\ 7,8 \\ 8,8 \\-----------------");
		System.out.println("------------/ 3,7 / 4,7 / 5,7 | 6,7 \\ 7,7 \\ 8,7 \\--------------");
		System.out.println("---------/ 2,6 / 3,6 / 4,6 / 5,6 \\ 6,6 \\ 7,6 \\ 8,6 \\-----------");
		System.out.println("------/ 1,5 / 2,5 / 3,5 / 4,5 | 5,5 \\ 6,5 \\ 7,5 \\ 8,5 \\--------");
		System.out.println("----| 0,4 | 1,4 | 2,4 | 3,4 | 4,4 | 5,4 | 6,4 | 7,4 | 8,4 |----");
		System.out.println("------\\ 0,3 \\ 1,3 \\ 2,3 \\ 3,3 | 4,3 / 5,3 / 6,3 / 7,3 /--------");
		System.out.println("---------\\ 0,2 \\ 1,2 \\ 2,2 \\ 3,2 / 4,2 / 5,2 / 6,2 /-----------");
		System.out.println("------------\\ 0,1 \\ 1,1 \\ 2,1 | 3,1 / 4,1 / 5,1 /--------------");
		System.out.println("---------------\\ 0,0 \\ 1,0 \\ 2,0 / 3,0 / 4,0 /-----------------");
	}
	/**
	 * This is printing the board with the current marble positions.
	 * It uses getField with the coordinates to get the required field and toString to display the first letter.
	 */
	
	public void printBoard() {
		System.out.println("---------------/ "+ getField(4, 8).toString() +" / "+ getField(5, 8).toString() +" / "+ getField(6, 8).toString() +" \\ "+ getField(7, 8).toString() +" \\ "+ getField(8, 8).toString() +" \\---------------");
		System.out.println("------------/ "+ getField(3, 7).toString() +" / "+ getField(4, 7).toString() +" / "+ getField(5, 7).toString() +" | "+ getField(6, 7).toString() +" \\ "+ getField(7, 7).toString() +" \\ "+ getField(8, 7).toString() +" \\--------------");
		System.out.println("----------/ "+ getField(2, 6).toString() +" / "+ getField(3, 6).toString() +" / "+ getField(4, 6).toString() +" / "+ getField(5, 6).toString() +" \\ "+ getField(6, 6).toString() +" \\ "+ getField(7, 6).toString() +" \\ "+ getField(8, 6).toString() +" \\------------");
		System.out.println("--------/ "+ getField(1, 5).toString() +" / "+ getField(2, 5).toString() +" / "+ getField(3, 5).toString() +" / "+ getField(4, 5).toString() +" | "+ getField(5, 5).toString() +" \\ "+ getField(6, 5).toString() +" \\ "+ getField(7, 5).toString() +" \\ "+ getField(8, 5).toString() +" \\----------");
		System.out.println("------| "+ getField(0, 4).toString() +" | "+ getField(1, 4).toString() +" | "+ getField(2, 4).toString() +" | "+ getField(3, 4).toString() +" | "+ getField(4, 4).toString() +" | "+ getField(5, 4).toString() +" | "+ getField(6, 4).toString() +" | "+ getField(7, 4).toString() +" | "+ getField(8, 4).toString() +" |--------");
		System.out.println("--------\\ "+ getField(0, 3).toString() +" \\ "+ getField(1, 3).toString() +" \\ "+ getField(2, 3).toString() +" \\ "+ getField(3, 3).toString() +" | "+ getField(4, 3).toString() +" / "+ getField(5, 3).toString() +" / "+ getField(6, 3).toString() +" / "+ getField(7, 3).toString() +" /----------");
		System.out.println("----------\\ "+ getField(0, 2).toString() +" \\ "+ getField(1, 2).toString() +" \\ "+ getField(2, 2).toString() +" \\ "+ getField(3, 2).toString() +" / "+ getField(4, 2).toString() +" / "+ getField(5, 2).toString() +" / "+ getField(6, 2).toString() +" /------------");
		System.out.println("------------\\ "+ getField(0, 1).toString() +" \\ "+ getField(1, 1).toString() +" \\ "+ getField(2, 1).toString() +" | "+ getField(3, 1).toString() +" / "+ getField(4, 1).toString() +" / "+ getField(5, 1).toString() +" /--------------");
		System.out.println("--------------\\ " + getField(0, 0).toString() + " \\ " + getField(1, 0).toString() + " \\ " + getField(2, 0).toString() + " / "+ getField(3, 0).toString() +" / "+ getField(2, 0).toString() +" /----------------");
	}
	
}