package abalone;

import abaloneServer.ClientHandler;

public class Player {
	
	protected Colour colour;
	protected String name;
	private Game game;
	private int pushedMarbles = 0;
	private ClientHandler client;
	
	/**
	 * The constructor of player. I
	 * @param colour the colour a player plays with.
	 * @param name the name of the player.
	 * @param client so you can call the client from a player list from a server.
	 */
	
	public Player(Colour colour, String name, ClientHandler client) {
		this.colour = colour;
		this.name = name;
		this.client = client;
	}
	
	public ClientHandler getClient() {
		return client;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	public Colour getColour() {
		return colour;
	}
	
	/**
	 * This method will give you the first letter of the colour of the player.
	 * This is used as an abbreviation.
	 * @return first letter of the colour of the player.
	 */
	
	public String getOneLetterColour() {
		switch (colour) {
			case BLACK:
				return "B";
			case RED:
				return "R";
			case WHITE:
				return "W";
			case YELLOW:
				return "Y";
			default:
				return null;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public Game getGame() {
		return game;
	}
	
	public Move makeMove(Game game) {
		System.out.println("Wrong one");
		return null;
	}
	
	public void incPushedMarbles() {
		pushedMarbles++;
	}
	
	public int getPushedMarbles() {
		return pushedMarbles;
	}
	
}