package abalone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * Game class keeps track general statistics in the game.
 * 
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */

public class Game {
	
	private int playerAmount;
	private String gamename;
	private Board board;
	private Player[] players;
	private Map<Colour, Player> colourmap = new HashMap<Colour, Player>();
	private int movesMade = 0;
	
	
	/**
	 * This stores the name of the game and the amount of players.
	 * 
	 * @param gamename is used for the protocol to give every game their own name. 
	 * 		  So you can connect to other games using their name
	 * @param players gives the amount of players playing the game.
	 * 		  This controls the different layout the marbles are laid in.
	 * @requires gamename != null;
	 * @requires players != null;
	 */
	public Game(String gamename, Player[] players) {
		
		this.gamename = gamename;
		this.playerAmount = players.length;
		this.players = players;
		
		//To get the player object if only the colour is known.
		for (Player player : players) {
			if (player != null) {
				colourmap.put(player.getColour(), player);
			}
		}
		
		board = new Board(playerAmount);	//initialize the board for the amount of players
	}
	
	/**
	 * Create a deepcopy used in strategy. 
	 * So that moves can be tested without moving marbles on the real board.
	 * @return deepcopy of the game
	 */
	
	public Game deepCopy() {
		Game newGame = new Game(gamename, players);
		newGame.movesMade = movesMade;
		newGame.board = board.deepCopy();
		return newGame;
	}
	
	/**
	 * Start the game.
	 * This will also keep track of if there is a winner or draw.
	 */
	
	public void startGame() {
		board.printBoard();
		//if there are 4 players, player WHITE and player RED should be swapped to play with the correct order
		if (players.length == 4) {
			Player temp = players[1];
			players[1] = players[2];
			players[2] = temp;
		}
		while (getWinner().size() == 0 && get40Moves() == null) {
			for (Player player : players) {
				Move move = player.makeMove(this);
				if (isValidMove(move)) {
					makeMove(move);
					board.printBoard();
				} else {
					while (!isValidMove(move)) {
						board.printBoard();
						System.out.println("Invalid move, try again.");
						move = player.makeMove(this);
					}
				}
			}
		}
		if (get40Moves() != null) {
			System.out.println(get40Moves());
		} else {
			if (getWinner().size() == 1) {
				for (Player player : getWinner()) {
					System.out.println(player.getName() + " has won!");
				}
			} else if (getWinner().size() == 2) {
				List<Player> result = new ArrayList<Player>();
				for (Player player : getWinner()) {
					result.add(player);
				}
				System.out.println(result.get(0) + " and " + result.get(1) + " have won!");
			}
		}
	}
	
	public Board getBoard() {
		return board;
	}
	
	public Map<Colour, Player> getColourMap() {
		return colourmap;
	}
	
	public int getPlayerAmount() {
		return playerAmount;
	}
	
	/**
	 * Makes the move by first checking if the move is valid.
	 * @param move the move to be made.
	 */
	public void makeMove(Move move) {
		
		movesMade++;
		
		int counterPushAmount = counterPushAmount(move);
		
		// If counterPushAmount(move) > 0, the return set always contains only one item. 
		// This is why the next two block run linear eventhough there are for loops.
		// Finds the last the field behind the marbles being pushed,
		// where an marble can be placed if there is a field, otherwise the marble is lost. 
		if (counterPushAmount == 1) {
			for (Field field : getDirectionField(move)) {
				Marble marble1 = board.getField(field).getMarble();
				board.getField(field).empty();
				for (Field field2 : getDirectionField(new Move(field, field, move.getDirection(), move.getColour()))) {
					if (board.isField(field2)) {
						board.getField(field2).addMarble(marble1);
					} else {
						colourmap.get(move.getColour()).incPushedMarbles();
					}
				}
			}
		}
		
		if (counterPushAmount == 2) {
			for (Field field : getDirectionField(move)) {
				Marble marble1 = board.getField(field).getMarble();
				board.getField(field).empty();
				for (Field field2 : getDirectionField(new Move(field, field, move.getDirection(), move.getColour()))) {
					Marble marble2 = board.getField(field2).getMarble();
					board.getField(field2).empty();
					board.getField(field2).addMarble(marble1);;
					for (Field field3 : getDirectionField(new Move(field2, field2, move.getDirection(), move.getColour()))) {;
						if (board.isField(field3)) {
							board.getField(field3).addMarble(marble2);
						} else {
							colourmap.get(move.getColour()).incPushedMarbles();
						}
					}
				}
			}
		}
		
		if (move.getFront() != null) {
			Marble marbleFront = board.getField(move.getFront()).getMarble();
			board.getField(move.getFront()).empty();
			for (Field field : getDirectionField(move)) {
				board.getField(field).addMarble(marbleFront);
			}
				
			if (move.amountMarbles() == 3) {
				Marble marbleMiddle = board.getField(move.getMiddleField()).getMarble();
				board.getField(move.getFront()).addMarble(marbleMiddle);
				board.getField(move.getMiddleField()).empty();
				Marble marbleBack = board.getField(move.getBack()).getMarble();
				board.getField(move.getBack()).empty();
				board.getField(move.getMiddleField()).addMarble(marbleBack);;
			}
				
			if (move.amountMarbles() == 2) {
				Marble marbleBack = board.getField(move.getBack()).getMarble();
				board.getField(move.getBack()).empty();
				board.getField(move.getFront()).addMarble(marbleBack);
			}
				
		} else {
			for (Field field : getDirectionField(move)) {
				getBoard().getField(field).addMarble(new Marble(move.getColour()));
			}
			getBoard().getField(move.getField1()).empty();
			getBoard().getField(move.getField2()).empty();
			if (move.amountMarbles() == 3) {
				getBoard().getField(move.getMiddleField()).empty();
			}
		}
		
	}
	
	/**
	 * This method will give the set of player(s) that won the game.
	 * This method is used below 40 player moves. Because after that the game stops.
	 * When this happens the winners are determined in a different way.
	 * @return the winning player
	 */
	
	public Set<Player> getWinner() {
		Set<Player> result = new HashSet<Player>();
		if (players.length < 4) {
			for (Player player : players) {
				if (player.getPushedMarbles() > 5) {
					result.add(player);
					return result;
				}
			}
		}
		if (players.length == 4) {
			if (colourmap.get(Colour.BLACK).getPushedMarbles() + colourmap.get(Colour.WHITE).getPushedMarbles() > 5) {
				result.add(colourmap.get(Colour.BLACK));
				result.add(colourmap.get(Colour.WHITE));
			}
			if (colourmap.get(Colour.YELLOW).getPushedMarbles() + colourmap.get(Colour.RED).getPushedMarbles() > 5) {
				result.add(colourmap.get(Colour.YELLOW));
				result.add(colourmap.get(Colour.RED));
			}
		}
		return result;
	}
	
	/**
	 * This method calculates if there is a winner or a draw. 
	 * It is used when there are 40 moves and not yet 6 pushed marbles.
	 * When there are winner(s) it will return these players with a message that they won.
	 * @return Either the winning player(s) or that is a draw
	 */
	
	public String get40Moves() {
		if (movesMade > 39) {
			if (players.length == 2) {
				if (players[0].getPushedMarbles() > players[1].getPushedMarbles()) {
					return players[0].getName() + " won!";
				} else if (players[0].getPushedMarbles() < players[1].getPushedMarbles()) {
					return players[1].getName() + " won!";
				} else {
					return "It's a draw!";
				}
			} else if (players.length == 3) {
				int highest = 0;
				for (Player player : players) {
					if (player.getPushedMarbles() > highest) {
						highest = player.getPushedMarbles();
					}
				}
				List<Player> winners = new ArrayList<Player>();
				for (Player player : players) {
					if (player.getPushedMarbles() == highest) {
						winners.add(player);
					}
				}
				if (winners.size() == 3) {
					return "It's a draw!";
				} else if (winners.size() == 2) {
					return winners.get(0).getName() + " and " + winners.get(1).getName() + " have the same amount of points!";
				} else {
					return winners.get(0).getName() + " has won!";
				}
			} else if (players.length == 4) {
				if (players[0].getPushedMarbles() + players[1].getPushedMarbles() > players[2].getPushedMarbles() + players[3].getPushedMarbles()) {
					return players[0].getName() + " and " + players[1].getName() + " won!";
				} else if (players[0].getPushedMarbles() + players[1].getPushedMarbles() < players[2].getPushedMarbles() + players[3].getPushedMarbles()) {
					return players[2].getName() + " and " + players[3].getName() + " won!";
				} else {
					return "It's a draw!";
				}
			}
		}
		return null;
	}
	
	/**
	 * This method takes a move and goes through various checks to see if it is a valid move.
	 * @param move the move which has to be validated
	 * @return false when move is invalid and true when it is valid
	 */
	
	public boolean isValidMove(Move move) {
		Colour colour = move.getColour();
		Colour teamColour = move.getTeamColour(playerAmount);
		int x1 = move.getField1().getX();
		int y1 = move.getField1().getY();
		int x2 = move.getField2().getX();
		int y2 = move.getField2().getY();
		
		//invalid choice of marbles: not in a line or range is too big etc.
		if (move.amountMarbles() == -1) {
			return false;
		}
		
		//checks if the fields exist 
		if (!board.isField(x1, y1) || !board.isField(x2, y2)) {
			return false;
		}
		
		//checks if the fields contain a marble
		if (board.getField(x1, y1).getEmpty() || board.getField(x2, y2).getEmpty()) {
			return false;
		}
		
		if (move.amountMarbles() == 3) {
			// check if the middle marble exists
			if (board.getField(move.getMiddleField()).getEmpty()) {
				return false;
			}
			
			//checks middle marble for correct colour
			if (!(board.getField(move.getMiddleField()).getMarble().getColour() == colour || board.getField(move.getMiddleField()).getMarble().getColour() == teamColour)) {
				return false;
			}
		} 
		
		//checks colour of field1
		if (!(board.getField(move.getField1()).getMarble().getColour() == colour || board.getField(move.getField1()).getMarble().getColour() == teamColour)) {
			return false;
		}
		
		//checks colour of field2
		if (!(board.getField(move.getField2()).getMarble().getColour() == colour || board.getField(move.getField2()).getMarble().getColour() == teamColour)) {
			return false;
		}
		
		if (move.getFront() != null) {
			//checks the colour of the pushing marble
			if (board.getField(move.getBack()).getMarble().getColour() != colour) {
				return false;
			}
		}

		//checks if direction points towards valid field
		if (!isValidDirection(move)) {
			return false;
		}
		
		//checks if the field(s) in the direction is/are empty, then the move is valid.
		if (isEmptyDirection(move)) {
			return true;
		}
		
		for (Field field : getDirectionField(move)) {
			if ((!board.getField(field).getEmpty()) && board.getField(field).getMarble().getColour() == colour ) {
				return false;
			}
			if (!board.getField(field).getEmpty() && board.getField(field).getMarble().getColour() == teamColour ) {
				return false;
			}
		}
		
		// must push but has no pushing power if == -1
		if (counterPushAmount(move) == -1) {
			return false;
		}
		
		if (counterPushAmount(move) >= move.amountMarbles()) {
			return false;
		}
		
		return true;
	}
		
	/**
	 * This method takes a move and validates the direction it wants to move in.
	 * @param move the move of which the direction needs to be validated.
	 * @return
	 */
	
	public boolean isValidDirection(Move move) {
		for (Field field : getDirectionField(move)) {
			if (!board.isField(field)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * This method takes a move and sees how much pushing power it would need to make it.
	 * @param move the move to be made. 
	 * @return the amount of pushing power you need to make a move. Maximum a player can have is 2.
	 * 	If it pushes sideways there is -1 pushing power because a sideways move can't push.
	 */
	
	public int counterPushAmount(Move move) {
		//Not possible to push if you push sideways
		if (move.getFront() == null) {
			return -1;
		}
		Direction direction = move.getDirection();
		Colour colour = move.getColour();
		
		for (Field field : getDirectionField(move)) {
			if (board.isField(field) && !board.getField(field).getEmpty() && board.getField(field).getMarble().getColour() != colour) {
				Move move2 = new Move(field, field, direction, colour);
				for (Field field2 : getDirectionField(move2)) {
					if (board.isField(field2) && !board.getField(field2).getEmpty() && board.getField(field2).getMarble().getColour() != colour) {
						Move move3 = new Move(field2, field2, direction, colour);
						for (Field field3 : getDirectionField(move3)) {
							if (board.isField(field3) && !board.getField(field3).getEmpty() && board.getField(field3).getMarble().getColour() != colour) {
								return 3;
							}
						}
						return 2;
					}
				}
				return 1;
			}
			return 0;
		}
		return -1;
	}
			
	/**
	 * This method takes a move and uses it's direction to get all fields in which the direction wants to go.
	 * @param move the move direction
	 * @return all the fields where the direction wants to go.
	 */
	
	public Set<Field> getDirectionField(Move move) {
		
		if (move.amountMarbles() == -1) {
			return null;
		}
		
		Set<Field> result = new HashSet<Field>();
		switch (move.getDirection()) {
			case EE:
				if (move.getFront() != null) {
					result.add(new Field(move.getFront().getX() + 1, move.getFront().getY()));
					return result;
				}
				if (move.amountMarbles() >= 1) {
					result.add(new Field(move.getField1().getX() + 1, move.getField1().getY()));
				} 
				if (move.amountMarbles() >= 2) {
					result.add(new Field(move.getField2().getX() + 1, move.getField2().getY()));
				} 
				if (move.amountMarbles() == 3) {
					result.add(new Field(move.getMiddleFieldX() + 1, move.getMiddleFieldY()));
				}
				return result;
			case NE:
				if (move.getFront() != null) {
					result.add(new Field(move.getFront().getX() + 1, move.getFront().getY() + 1));
					return result;
				}
				if (move.amountMarbles() >= 1) {
					result.add(new Field(move.getField1().getX() + 1, move.getField1().getY() + 1));
				} 
				if (move.amountMarbles() >= 2) {
					result.add(new Field(move.getField2().getX() + 1, move.getField2().getY() + 1));
				} 
				if (move.amountMarbles() == 3) {
					result.add(new Field(move.getMiddleFieldX() + 1, move.getMiddleFieldY() + 1));
				}
				return result;
			case NW:
				if (move.getFront() != null) {
					result.add(new Field(move.getFront().getX(), move.getFront().getY() + 1));
					return result;
				}
				if (move.amountMarbles() >= 1) {
					result.add(new Field(move.getField1().getX(), move.getField1().getY() + 1));
				} 
				if (move.amountMarbles() >= 2) {
					result.add(new Field(move.getField2().getX(), move.getField2().getY() + 1));
				} 
				if (move.amountMarbles() == 3) {
					result.add(new Field(move.getMiddleFieldX(), move.getMiddleFieldY() + 1));
				}
				return result;
			case SE:
				if (move.getFront() != null) {
					result.add(new Field(move.getFront().getX(), move.getFront().getY() - 1));
					return result;
				}
				if (move.amountMarbles() >= 1) {
					result.add(new Field(move.getField1().getX(), move.getField1().getY() - 1));
				} 
				if (move.amountMarbles() >= 2) {
					result.add(new Field(move.getField2().getX(), move.getField2().getY() - 1));
				} 
				if (move.amountMarbles() == 3) {
					result.add(new Field(move.getMiddleFieldX(), move.getMiddleFieldY() - 1));
				}
				return result;
			case SW:
				if (move.getFront() != null) {
					result.add(new Field(move.getFront().getX() - 1, move.getFront().getY() - 1));
					return result;
				}
				if (move.amountMarbles() >= 1) {
					result.add(new Field(move.getField1().getX() - 1, move.getField1().getY() - 1));
				} 
				if (move.amountMarbles() >= 2) {
					result.add(new Field(move.getField2().getX() - 1, move.getField2().getY() - 1));
				} 
				if (move.amountMarbles() == 3) {
					result.add(new Field(move.getMiddleFieldX() - 1, move.getMiddleFieldY() - 1));
				}
				return result;
			case WW:
				if (move.getFront() != null) {
					result.add(new Field(move.getFront().getX() - 1, move.getFront().getY()));
					return result;
				}
				if (move.amountMarbles() >= 1) {
					result.add(new Field(move.getField1().getX() - 1, move.getField1().getY()));
				} 
				if (move.amountMarbles() >= 2) {
					result.add(new Field(move.getField2().getX() - 1, move.getField2().getY()));
				} 
				if (move.amountMarbles() == 3) {
					result.add(new Field(move.getMiddleFieldX() - 1, move.getMiddleFieldY()));
				}
				return result;
			default:
				break;
		}
		return result;
	}
	
	/**
	 * This method will take a move and see if the direction it wants to go is empty or not.
	 * True if the fields are empty.
	 * @param move the move it wants to make.
	 * @return true/false depending on if the direction the move wants to go is empty or not.
	 */
	
	public boolean isEmptyDirection(Move move) {
		
		for (Field field : getDirectionField(move)) {
			if (!board.getField(field).getEmpty()) {
				return false;
			}
		}
		return true;
	}

}


	
	
	
	
	
	
	
	
	
	
	