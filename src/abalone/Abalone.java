package abalone;


public class Abalone {
	
	/**
	 * Start the general game with 2 players; Bas and Tim.
	 * You can use HumanPlayer or ComputerPlayer. ComputerPlayer will use our strategy to play automatically.
	 */
	
	public static void main(String[] args) {
		
		Player[] players = {new ComputerPlayer(Colour.WHITE, "Bas"), new ComputerPlayer(Colour.BLACK, "Tim")};
		Game game = new Game("NewGame", players);
		game.startGame();
		
	}
	
}