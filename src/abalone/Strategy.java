package abalone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Strategy {
	
	private Game game;
	private Colour colour;
	private Player player;
	
	private int moveCounter = 1;
	
	public Strategy(Colour colour) {
		this.colour = colour;
		
	}
	/**
	 * This method is the strategy used to make moves by ComputerPlayer.
	 * It has 5 basic first moves to give it an advantage.
	 * After this it will go through all of it's marbles and see if it can push any marble of the board.
	 * If this is possible it does this.
	 * Otherwise it make a random move.
	 * @param game the game in which a move has to be determined.
	 * @return the move
	 */
	
	public Move determineMove(Game game) {
		
		this.game = game;
		this.player = game.getColourMap().get(colour);
		
		if (game.getPlayerAmount() != 2) {
			moveCounter = 6;
		}
		
		Board board = game.getBoard();
		HashMap<Integer, Field> boardMap = board.getBoardMap();
		
		//Basic first moves made if the CPU is white.
		if (game.getPlayerAmount() == 2 && colour == Colour.WHITE) {
			
			if (moveCounter == 1) {
				moveCounter++;
				return new Move(new Field(2, 2), new Field(4, 2), Direction.NE, colour);
			}
			
			if (moveCounter == 2) {
				moveCounter++;
				return new Move(new Field(1, 1), new Field(3, 1), Direction.NE, colour);
			}
			
			if (moveCounter == 3) {
				moveCounter++;
				return new Move(new Field(0, 0), new Field(2, 0), Direction.NE, colour);
			}
			
			if (moveCounter == 4) {
				moveCounter++;
				return new Move(new Field(3, 0), new Field(4, 1), Direction.NE, colour);
			}
			if (moveCounter == 5) {
				moveCounter++;
				return new Move(new Field(2, 1), new Field(2, 2), Direction.NW, colour);
			}
		}
		
		//Basic first moves the CPU makes if it is black.
		if (game.getPlayerAmount() == 2 && colour == Colour.BLACK) {
			
			if (moveCounter == 1) {
				moveCounter++;
				return new Move(new Field(4, 6), new Field(6, 6), Direction.SW, colour);
			}
			
			if (moveCounter == 2) {
				moveCounter++;
				return new Move(new Field(5, 7), new Field(7, 7), Direction.SW, colour);
			}
			
			if (moveCounter == 3) {
				moveCounter++;
				return new Move(new Field(6, 8), new Field(8, 8), Direction.SW, colour);
			}
			
			if (moveCounter == 4) {
				moveCounter++;
				return new Move(new Field(5, 8), new Field(4, 7), Direction.SW, colour);
			}
			if (moveCounter == 5) {
				moveCounter++;
				return new Move(new Field(6, 6), new Field(6, 7), Direction.SE, colour);
			}
		}
	
		//creates a set with all Fields
		List<Field> allFields = new ArrayList<Field>();
		for (Integer integer : boardMap.keySet()) {
			allFields.add(boardMap.get(integer));
		}
		
		//creates a set with all directions
		List<Direction> allDirections = new ArrayList<Direction>();
		allDirections.add(Direction.EE);
		allDirections.add(Direction.NE);
		allDirections.add(Direction.NW);
		allDirections.add(Direction.SE);
		allDirections.add(Direction.SW);
		allDirections.add(Direction.WW);
		
		//The strategy needs to make the moves to test them out but not on the actual board.
		//So it makes a deepCopy to test out all of the moves.
		Game gameCopy = game.deepCopy();
				
		for (Field field : allFields) {
			if (!field.getEmpty() && field.getMarble().getColour() == colour) {
				for (Field field2 : allFields) {
					if (!field2.getEmpty() && field2.getMarble().getColour() == colour) {
						for (Direction direction : allDirections) {
							Move move = new Move(field, field2, direction, colour);
							if (game.isValidMove(move) && game.counterPushAmount(move) > 0) {
								int pushedMarbles = player.getPushedMarbles();
								gameCopy.makeMove(move);
								if (gameCopy.getColourMap().get(colour).getPushedMarbles() > pushedMarbles) {
									moveCounter++;
									return move;
								} else {
									gameCopy = game.deepCopy();
								}
							}
						}
					}
				}
			}
		}
		
		Move move;
		
		while (true) {
			int random = (int) (Math.random() * allFields.size());
			Field field1 = allFields.get(random);
			int random2 = (int) (Math.random() * allFields.size());
			Field field2 = allFields.get(random2);
			int random3 = (int) (Math.random() * allDirections.size());
			Direction direction = allDirections.get(random3);
			
			move = new Move(field1, field2, direction, colour);
			
			if (!field1.getEmpty() && field1.getMarble().getColour() == colour && !field2.getEmpty() && field2.getMarble().getColour() == colour && game.isValidMove(move)) {
				break;
			}
		}
		
		moveCounter++;

		return move;
	}
	
}