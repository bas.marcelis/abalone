package abalone;

/**
 * This give the possible colours of all players.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */

public enum Colour {
BLACK,
WHITE,
RED,
YELLOW
}