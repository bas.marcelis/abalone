package abalone;

public class Marble {
	
	private Colour colour;
	
	public Marble(Colour colour) {
		this.colour = colour;
	}
	
	public Colour getColour() {
		return colour;
	}

	public Marble deepCopy() {
		return new Marble(colour);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Marble other = (Marble) obj;
		if (colour != other.colour) {
			return false;
		}
		return true;
	}

	/**
	 * This method will take the colour of the player and return the first letter in caps.
	 * This is used when printing the board.
	 * @return the first letter of the player's colour.
	 */
	
	public String toString() {
		switch (colour) {
			case BLACK:
				return "B";
			case RED:
				return "R";
			case WHITE:
				return "W";
			case YELLOW:
				return "Y";
			default:
				return "O";
		}
	}

}




