package abalone;

/**
 * The directions in which a marble can move.
 * @author Bas Marcelis s2329204 and Tim Uilkema s2385694
 *
 */

public enum Direction {
NW,
NE,
EE,
SE,
SW,
WW
}